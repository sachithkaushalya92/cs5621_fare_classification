import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression


def read_data_set(file_name, is_train):
    df = read_file(file_name)

    if is_train:
        df.label = [1 if each == 'correct' else 0 for each in df.label]

    return df


def read_file(file_name):
    return pd.read_csv(file_name)


def write_result_to_file(data_set):
    data_set.to_csv('final_result.csv', index=False)


def logistic_regression(train_data, test_data, sample_data):
    y_train = train_data['label']
    trip_id = test_data['tripid']

    x_train = train_data.drop(['tripid', 'label', 'additional_fare', 'pickup_time', 'drop_time'], axis=1)
    x_train = x_train.replace(to_replace=np.nan, value=0)

    x_test = test_data.drop(['tripid', 'additional_fare', 'pickup_time', 'drop_time'], axis=1)
    x_test = x_test.replace(to_replace=np.nan, value=0)

    model = LogisticRegression(C=100, random_state=42)
    model.fit(x_train, y_train)

    y_pred = model.predict(x_test)

    sample_data['tripid'] = trip_id
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('train.csv', True)
    test_data_set = read_data_set('test.csv', False)
    sample_data_set = read_file('final_result.csv')

    logistic_regression(train_data_set, test_data_set, sample_data_set)


create_model()
